package exceptionII;

public class IllegalEmployeeWeightException extends EmployeeException {

    public IllegalEmployeeWeightException(String message) {
        super(message);
    }
}
