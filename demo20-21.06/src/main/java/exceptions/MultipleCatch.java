package exceptionI;

class MultipleCatch {
    public static void main(String args[]){
        int a[]=new int[7];
        try{

            //a[2]=30/0;
            //a[89]=30/1;
            a[89]=30/0;

            System.out.println("First print statement in try block");
        }

        catch(ArithmeticException e){
            System.out.println("Warning: ArithmeticException");
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Warning: ArrayIndexOutOfBoundsException");
        }
        catch(Exception e){
            System.out.println("Warning: Some Other exception");
        }
        finally{
            System.out.println("Finally...");
        }
        System.out.println("Out of try-catch block...");
    }
}