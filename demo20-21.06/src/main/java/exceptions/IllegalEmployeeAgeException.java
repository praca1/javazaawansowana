package exceptionII;

public class IllegalEmployeeAgeException extends EmployeeException {

    public IllegalEmployeeAgeException(String message) {
        super(message);
    }
}