package exceptionII;

public class IllegalEmployeeNameException extends EmployeeException {

    public IllegalEmployeeNameException(String message) {
        super(message);
    }
}