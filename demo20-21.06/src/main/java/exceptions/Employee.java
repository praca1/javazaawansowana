package exceptionII;

public class Employee {

    private int age;
    private int weight;
    private String name;

    public Employee(int age, int weight, String name) throws EmployeeException{
        if (age < 18) {
            throw new IllegalEmployeeAgeException("Wiek pracownika za niski");
        }
        if (weight < 30 || weight > 100) {
            throw new IllegalEmployeeWeightException("Niepoprawna waga");
        }
        if (name == null || name.isEmpty()) {
            throw new IllegalEmployeeNameException("Niepoprawna nazwa");
        }
        this.age = age;
        this.weight = weight;
        this.name = name;
    }


}
