package exceptionII;

public abstract class EmployeeException extends Exception {

    public EmployeeException(String message) {
        super(message);
    }
}