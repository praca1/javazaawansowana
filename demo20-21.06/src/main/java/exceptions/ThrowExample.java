package exceptionI;

import java.io.FileNotFoundException;

public class ThrowExample {


    public void testMethod() throws FileNotFoundException {
            throw new FileNotFoundException("test");
    }

    public void testMethodII() throws FileNotFoundException {
 this.testMethod();
    }

    public void testMethodIII() {
        try {
            this.testMethodII();
        } catch (FileNotFoundException e) {
            System.out.println("Wyjatek obsluzony");

        }
    }





}
