package interfaces;

import interfaces.OperateCar;

public class CarImpl implements OperateCar {


    @Override
    public int turn(String direction, double radius, double startSpeed, double endSpeed) {
        return 0;
    }

    @Override
    public int changeLanes(String direction, double startSpeed, double endSpeed) {
        return 0;
    }

    @Override
    public int signalTurn(String direction, boolean signalOn) {
        return 0;
    }

    @Override
    public int getRadarFront(double distanceToCar, double speedOfCar) {
        return 0;
    }

    @Override
    public int getRadarRear(double distanceToCar, double speedOfCar) {
        return 0;
    }
}
