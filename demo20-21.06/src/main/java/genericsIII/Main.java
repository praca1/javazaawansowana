package genericsIII;


import genericsII.Apple;
import genericsII.Orange;
import genericsII.PolishApple;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        //Basic implementation
        processElements(new ArrayList<Object>());
     //      processElements(new ArrayList<Integer>());
    //      processElements(new ArrayList<Double>());

        //Improved implementation
        processElementsImproved(new ArrayList<Object>());
        processElementsImproved(new ArrayList<Integer>());
        processElementsImproved(new ArrayList<Double>());

        //Extends
        processElementsExtends(new ArrayList<Apple>());
        processElementsExtends(new ArrayList<PolishApple>());
   //     processElementsExtends(new ArrayList<Orange>());

        //Super
        processElementsSuper(new ArrayList<Apple>());
        processElementsSuper(new ArrayList<PolishApple>());
    //    processElementsSuper(new ArrayList<Orange>());




    }

    public static void processElements(List<Object> elements){
        for(Object o : elements){
            System.out.println(o.toString());
        }
    }

    public static void processElementsImproved(List<?> elements){
        for(Object o : elements){
            System.out.println(o.toString());
        }
    }

    public static void processElementsExtends(List<? extends Apple> elements){
        for(Object o : elements){
            System.out.println(o.toString());
        }
    }

    public static void processElementsSuper(List<? super PolishApple> elements){
        for(Object o : elements){
            System.out.println(o.toString());
        }
    }
}
