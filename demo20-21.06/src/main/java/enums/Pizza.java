package enums;

import enums.PizzaStatus;

public class Pizza {

    private PizzaStatus status;

    public boolean isDeliverable() {
        return this.status.isReady();
    }

    public Pizza(PizzaStatus status) {
        this.status = status;
    }

    public void printTimeToDeliver() {
        System.out.println("Time to delivery is " +
                this.status.getTimeToDelivery());
    }

    public static void testEnum(){

        PizzaStatus[] pizzaStatuses = PizzaStatus.values();
        for (PizzaStatus p : pizzaStatuses){
            System.out.println(p);
        }

        System.out.println(PizzaStatus.ORDERED.name());
        System.out.println(PizzaStatus.DEFAULT.ordinal());
        System.out.println(PizzaStatus.valueOf("DELIVERED"));

    }

    public static void main(String[] args) {

        Pizza p = new Pizza(PizzaStatus.DEFAULT);

        testEnum();
    }

}