package ioV;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FilesExample {
    public static void main(String[] args) {
        Path p1 = Paths.get("outfile.txt");
        Files.exists(p1);
        Files.notExists(p1);

        Files.isDirectory(p1);
        Files.isReadable(p1);
        Files.isRegularFile(p1);
        Files.isWritable(p1);
        Files.isExecutable(p1);


        try {
            Files.createFile(Paths.get("new.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Files.copy(p1, Paths.get("copy.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            Files.delete(p1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Files.move(p1, Paths.get("moved"));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
