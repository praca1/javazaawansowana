package composition;

public class FastCar {
    private Engine engine;
    private String color;


    public FastCar() {
        this.color="GREEN";
        this.engine = new Engine();
    }

    void start() {
        if (engine.checkOilLevel() < 20) {
            System.out.printf("Engine oil level to low.");
            return;
        }

        if (!engine.isRunning()) {
            engine.turnOn();
        }

        System.out.println("The car starts.");
    }

    void stop() {
        System.out.println("Car stops.");
        engine.turnOff();
    }
}
