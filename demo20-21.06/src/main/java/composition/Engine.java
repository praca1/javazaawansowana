package composition;

public class Engine {
    private int oilLevelPercent;
    private boolean isRunning;

    void turnOn() {
        isRunning = true;
        System.out.println("Engine start");
    }

    void turnOff() {
        isRunning = false;
        System.out.println("Engine shut down.");
    }

    int checkOilLevel() {
        return oilLevelPercent;
    }

    boolean isRunning() {
        return isRunning;
    }
}