package ioI;

import java.io.*;

public class FileStreamExample {
    public static void main(String[] args) {


        try {
            InputStream fileInputStream=new FileInputStream("file.txt");
            OutputStream fileOutputStream=new FileOutputStream("fileOut.txt");
            int i;
            while((i=fileInputStream.read())!=-1){
                System.out.println("Znak: "+(char)i+" reprezentowany przez bajt: "+i);
                fileOutputStream.write(i);
            }
            fileInputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
