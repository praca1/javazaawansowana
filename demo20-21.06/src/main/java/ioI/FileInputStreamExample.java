package ioI;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileInputStreamExample {
    public static void main(String[] args) {
        try {
            InputStream fileInputStream=new FileInputStream("file.txt");
            int b;
            while((b=fileInputStream.read())!=-1){
                System.out.print((char)b);
            }
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}