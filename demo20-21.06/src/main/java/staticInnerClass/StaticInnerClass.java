package staticInnerClass;

public class StaticInnerClass {
    public static void main(String[] args) {

        MotherBoard.USB usb = new MotherBoard.USB();
        System.out.println("Total Ports = " + usb.getTotalPorts());
    }
}