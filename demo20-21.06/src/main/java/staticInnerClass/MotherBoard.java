package staticInnerClass;

public class MotherBoard {

    // static nested class
    public static class USB{
        int usb2 = 2;
        int usb3 = 1;
        public int getTotalPorts(){
            return usb2 + usb3;
        }
    }

}
