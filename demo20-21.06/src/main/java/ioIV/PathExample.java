package ioIV;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathExample {
    public static void main(String[] args) {
       
       //tworzenie aciezki
        Path p1 = Paths.get("C:\\Users\\janek\\file.txt");
        Path p2 = Paths.get("/home/janek/file.txt");
        Path p3 = Paths.get(System.getProperty("user.home"));
        System.out.println(p3);

        //pobieranie informacji o ścieżce:
        Path path = Paths.get("C:\\Users\\janek\\file.txt");
        path.getFileName();    // file.txt
        path.getRoot();    // C:
        path.getName(0);   // Users
        path.getParent();    // C:\Users\janek
        path.subpath(0, 1);    // Users\janek

        //     konwersja do ścieżki bezwzględnej:
        Path path1 = Paths.get("file.txt");  // uruchamiane w C:\Users\janek
        path1.toAbsolutePath(); // C:\Users\janek\file.txt

        //laczenie sciezek
        Path pathA = Paths.get("C:\\Users\\janek");
        pathA.resolve("file.txt");    // C:\Users\janek\file.txt


        //     porównywanie ścieżek:
        Path path4 = Paths.get("C:\\Users\\janek");
        Path path5 = Paths.get("C:\\Users");
        path4.equals(path5);    // false
        path4.startsWith(path5);    // true
        path4.endsWith(path5);   // false

    }
}