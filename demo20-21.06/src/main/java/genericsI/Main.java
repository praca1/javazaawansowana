package genericsI;

import genericsI.apple.Apple;
import genericsI.apple.AppleBox;
import genericsI.orange.Orange;
import genericsI.orange.OrangeBox;

public class Main {
    public static void main(String[] args) {
        Apple a = new Apple();
        Orange o = new Orange();

        AppleBox appleBox = new AppleBox(a);
        OrangeBox orangeBox = new OrangeBox(o);

        FruitBox<Apple> fruitBoxApple = new FruitBox<>(a);
        FruitBox<Orange> fruitBoxOrange = new FruitBox<>(o);

        Banana b = new Banana();
        FruitBox<Banana> fruitBoxBanana = new FruitBox<>(b);

    }
}
