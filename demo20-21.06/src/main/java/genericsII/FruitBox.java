package genericsII;

public class FruitBox<T extends Apple> {
    private T fruit;

    public FruitBox(T fruit) {
        this.fruit = fruit;
    }

    public T getFruit() {
        return fruit;
    }

    public void setApple(T fruit) {
        this.fruit = fruit;
    }

    public int getData(){
        return fruit.getSugar();
    }
}