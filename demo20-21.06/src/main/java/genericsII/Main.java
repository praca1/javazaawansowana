package genericsII;


import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Apple a = new Apple();
        PolishApple ap = new PolishApple();
        Orange o = new Orange();


        FruitBox<Apple> fruitBoxApple = new FruitBox<>(a);
        FruitBox<Apple> fruitBoxPolishApple = new FruitBox<>(ap);

 //       FruitBox<Orange> fruitBoxOrange = new FruitBox<>(o);

    }
}
