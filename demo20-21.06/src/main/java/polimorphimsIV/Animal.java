package polimorphimsIV;

public interface Animal {
    void makeSound();
    void eat();
}
