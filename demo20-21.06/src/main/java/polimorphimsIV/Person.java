package polimorphimsIV;

public class Person {
    private Animal[] animals;
    private HomePet[] homePets;


    public Person() {

        animals = new Animal[2];
        animals[0]= new Cat();
        animals[1] = new Dog();

        homePets = new HomePet[2];
        homePets[0]= new Cat();
        homePets[1] = new Dog();
    }

    public void makeAnimalsEating(){
        for(Animal a : animals){
            a.eat();
        }
    }

    public void makeAnimalsPlaying(){
        for(HomePet p : homePets){
            p.play();
        }
    }


}
