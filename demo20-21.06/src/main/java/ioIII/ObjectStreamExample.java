package ioIII;

import java.io.*;

public class ObjectStreamExample {




    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Person person = new Person("TEST", 34);

        ObjectOutputStream objectOutputStream =
                new ObjectOutputStream(new FileOutputStream("person.bin"));



        objectOutputStream.writeObject(person);
        objectOutputStream.close();


        ObjectInputStream objectInputStream =
                new ObjectInputStream(new FileInputStream("person.bin"));

        Person personRead = (Person) objectInputStream.readObject();

        objectInputStream.close();

        System.out.println(personRead.getName());
        System.out.println(personRead.getAge());
    }
}