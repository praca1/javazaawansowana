package polimorphismII;

public class Main {
    public static void main(String[] args) {


        Kolo k = new Kolo();
        Trojkat t = new Trojkat();
        Prostokat p = new Prostokat();
        Deltoid d = new Deltoid();


        Figura[] figury = {k,t,p, d};

        for(Figura f : figury){
            f.obliczObwod();
            f.obliczPole();
        }

    }
}
