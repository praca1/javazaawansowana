package polimorphismII;

public interface Figura {
    int obliczPole();
    int obliczObwod();
}
