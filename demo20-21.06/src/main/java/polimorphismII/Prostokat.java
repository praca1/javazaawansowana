package polimorphismII;

public class Prostokat implements Figura{
    @Override
    public int obliczPole() {
        System.out.println("Obliczam pole dla Prostokata");
        return 0;
    }

    @Override
    public int obliczObwod() {
        System.out.println("Obliczam obwod dla Prostokata");
        return 0;
    }
}
