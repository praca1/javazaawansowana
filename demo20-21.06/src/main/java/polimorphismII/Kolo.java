package polimorphismII;

public class Kolo implements Figura{
    @Override
    public int obliczPole() {
        System.out.println("Obliczam pole dla Kola");
        return 0;
    }

    @Override
    public int obliczObwod() {
        System.out.println("Obliczam obwod dla Kola");
        return 0;
    }
}
