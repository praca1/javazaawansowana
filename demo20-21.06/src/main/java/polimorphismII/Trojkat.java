package polimorphismII;

public class Trojkat implements Figura {
    @Override
    public int obliczPole() {
        System.out.println("Obliczam pole dla Trójkąta");
        return 0;
    }

    @Override
    public int obliczObwod() {
        System.out.println("Obliczam obwod dla Trójkąta");
        return 0;
    }
}
