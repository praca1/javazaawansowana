package collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetExample {

    public static void main(String[] args) {

        //Tworzenie zbioru
        Set<String> hash_Set = new HashSet<String>();
        hash_Set.add("A");
        hash_Set.add("B");
        hash_Set.add("C");
        hash_Set.add("D");
        hash_Set.add("E");

        //Kopiowanie zawartosci seta
        Set<String> set2 = new HashSet<>();
        set2.addAll(hash_Set);
        System.out.println("Set2: " + set2);

        // Usuwanie elementów
        HashSet<Integer> numbers = new HashSet<>();
        numbers.add(2);
        numbers.add(5);
        numbers.add(6);
        System.out.println("HashSet: " + numbers);

        boolean value1 = numbers.remove(5);
        System.out.println("Is 5 removed? " + value1);

        boolean value2 = numbers.removeAll(numbers);
        System.out.println("Are all elements removed? " + value2);

        //Czesc wspolna zbiorow
        Set<Integer> primeNumbers = new HashSet<>();
        primeNumbers.add(2);
        primeNumbers.add(3);
        System.out.println("HashSet1: " + primeNumbers);

        Set<Integer> evenNumbers = new HashSet<>();
        evenNumbers.add(2);
        evenNumbers.add(4);
        System.out.println("HashSet2: " + evenNumbers);
        evenNumbers.retainAll(primeNumbers);
        System.out.println("Intersection is: " + evenNumbers);

        //Roznica zbiorow
        Set<Integer> primeNumbers1 = new HashSet<>();
        primeNumbers1.add(2);
        primeNumbers1.add(3);
        primeNumbers1.add(5);
        System.out.println("HashSet1: " + primeNumbers1);

        Set<Integer> oddNumbers = new HashSet<>();
        oddNumbers.add(1);
        oddNumbers.add(3);
        oddNumbers.add(5);
        System.out.println("HashSet2: " + oddNumbers);

        primeNumbers1.removeAll(oddNumbers);
        System.out.println("Difference : " + primeNumbers1);

        //Podzbior
        HashSet<Integer> numbers1 = new HashSet<>();
        numbers1.add(1);
        numbers1.add(2);
        numbers1.add(3);
        numbers1.add(4);
        System.out.println("HashSet1: " + numbers1);

        HashSet<Integer> primeNumbers2 = new HashSet<>();
        primeNumbers2.add(2);
        primeNumbers2.add(3);
        System.out.println("HashSet2: " + primeNumbers2);

        boolean result = numbers1.containsAll(primeNumbers2);
        System.out.println("Is HashSet2 is subset of HashSet1? " + result);


        //Iterowanie po elementach
      //  Iterator<Integer> iterate = numbers.iterator();
     //   System.out.print("HashSet using Iterator: ");
     //   while(iterate.hasNext()) {
     //       System.out.print(iterate.next());
     //       System.out.print(", ");
     //   }

        for(Integer i: numbers){
            System.out.printf(i.toString());
        }


    }
}
