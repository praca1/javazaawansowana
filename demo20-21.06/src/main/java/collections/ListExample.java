package collections;

import java.util.*;

public class ListExample {


    public static void main(String[] args) {

        // Zamiana tablicy na listę
        String[] vowels = {"a","e","i","o","u"};


        List<String> vowelsList = Arrays.asList(vowels);
        System.out.println(vowelsList);




        //Reczne kopiowanie elementow z tablicy do listy
        List<String> myList = new ArrayList<>();
        for(String s : vowels){
            myList.add(s);
        }
        System.out.println(myList);
        myList.clear();

        //Zamiana w drugą stronę
        List<String> letters = new ArrayList<String>();

        letters.add("A");
        letters.add("B");
        letters.add("C");

        String[] strArray = new String[letters.size()];
        strArray = letters.toArray(strArray);
        System.out.println(Arrays.toString(strArray));

        //Sortowanie listy
        List<Integer> ints = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10; i++)
            ints.add(random.nextInt(1000));

        //Sortowanie naturalne
        Collections.sort(ints);
        System.out.println("Natural Sorting: "+ints);

        //Wlasne kryteria sortowania
        ints.sort((o1,o2) -> {return (o2-o1);});
        System.out.println("Reverse Sorting: "+ints);

        //Podsatwowe operacje na listach
        List<String> characters= new ArrayList<String>();

        //dodawanie elementow
        characters.add("A");
        characters.add("I");

        //wstawianie elementu do listy na konkretną pozycję
        characters.add(1,"E");
        System.out.println(characters);

        List<String> list = new ArrayList<String>();
        list.add("O");
        list.add("U");

        //Łączenie list
        characters.addAll(list);
        System.out.println(characters);

        //Czyszczenie listy
        list.clear();

        //Pobieranie wielkości
        System.out.println("letters list size = "+characters.size());

        //zmiana wartosci na konkretnej pozycji
        characters.set(2, "E");
        System.out.println(characters);

        //sublista
        characters.clear();
        characters.add("E");
        characters.add("E");
        characters.add("I");
        characters.add("O");
        list = characters.subList(0, 2);
        System.out.println("letters = "+characters+", list = "+list);

        //klonowanie listy
        List<String> cloned_list
                = new ArrayList<String>(list);

        System.out.println(cloned_list);

    }

}



