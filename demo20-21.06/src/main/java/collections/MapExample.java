package collections;

import java.util.HashMap;
import java.util.Map;

public class MapExample {

    public static void main(String[] args) {

        ///Tworzenie tablicy
        Map<String, Integer> numbers = new HashMap<>();

        numbers.put("One", 1);
        numbers.put("Two", 2);
        System.out.println("Map: " + numbers);

        //Pobieranie zbiorow
        System.out.println("Keys: " + numbers.keySet());

        System.out.println("Values: " + numbers.values());

        System.out.println("Entries: " + numbers.entrySet());

        //Usuwanie wartosci
        int value = numbers.remove("One");

        System.out.println("Removed Value: " + value);

        //Iteracja po zbiorach
        HashMap<String, Integer> myMap = new HashMap<>();
        myMap.put("One", 1);
        myMap.put("Two", 2);
        myMap.put("Three", 3);
        System.out.println("HashMap: " + myMap);

        System.out.print("Entries: ");
        for(Map.Entry<String, Integer> entry: myMap.entrySet()) {
            System.out.print(entry);
            System.out.print(", ");
        }

        System.out.print("\nKeys: ");
        for(String key: myMap.keySet()) {
            System.out.print(key);
            System.out.print(", ");
        }

        System.out.print("\nValues: ");
        for(Integer myValue: myMap.values()) {
            System.out.print(myValue);
            System.out.print(", ");
        }
        //Sprawdzanie zawartosci
        HashMap<String, Integer> myMapNew = new HashMap<>();
        myMapNew.put("One", 1);
        myMapNew.put("Two", 2);
        myMapNew.put("Three", 3);

        System.out.println(myMapNew.containsKey("One"));
        System.out.println(myMapNew.containsValue(3));

        //Dodatkowe operacje
        System.out.println(myMapNew.size());
        myMapNew.clear();
        System.out.println(myMapNew.isEmpty());

    }

}
