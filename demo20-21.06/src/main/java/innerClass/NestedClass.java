package innerClass;

public class NestedClass {
        public static void main(String[] args) {

            // create object of Outer class CPU
            CPU cpu = new CPU();

            // create an object of inner class Processor using outer class
            CPU.Processor processor = cpu.new Processor();

            // create an object of inner class RAM using outer class CPU
            System.out.println("Processor Cache = " + processor.getCache());
        }
    }


