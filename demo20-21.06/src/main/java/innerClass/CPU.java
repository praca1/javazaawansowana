package innerClass;

public class CPU {
    double price;
    // nested class
    public class Processor{

        // members of nested class
        double cores;
        String manufacturer;

        public double getCache(){
            return price*4.3;
        }
    }

}

