package ioVI;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class SmallAmountFilesWriteExample {
    public static void main(String[] args) {

        Charset utf8 = StandardCharsets.UTF_8;
        try {
            Files.write(Paths.get("Plik0.txt"), "Brak informacji o kodowaniu ąęźż".getBytes());
            Files.write(Paths.get("Plik1.txt"), "Kodowanie utf-8 ąęźż".getBytes(utf8),
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}