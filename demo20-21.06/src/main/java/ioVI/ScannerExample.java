package ioVI;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerExample {
    public static void main(String args[]) throws FileNotFoundException {
        File plik = new File("example1.pl");
        Scanner in = new Scanner(plik);
        while(in.hasNextLine()) {
            String tekst = in.nextLine();
            System.out.println(tekst);
        }
    }
}