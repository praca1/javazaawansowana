package ioVI;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class SmallAmountFilesReadExample {

    public static void main(String[] args) throws Exception
    {
        List<String> data= Files.readAllLines(Paths.get("file.txt"));
        System.out.println(data);
    }
}