package abstraction;

abstract class GraphicObject {
    int x, y;
     void moveTo(int newX, int newY) {
        System.out.println("This GraphicObject will be moved to the following coordinates: "+x+", "+y);
         System.out.println(this.getClass());
     }
    protected abstract void draw();abstract void resize();
}