package abstraction;

class Rectangle extends GraphicObject {
    protected void draw() {
        System.out.println("Drawing Rectangle");
    }
    void resize() {
        System.out.println("Resizing Rectangle");
    }

}