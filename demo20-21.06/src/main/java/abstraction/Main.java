package abstraction;

public class Main {

    public static void main(String[] args) {
        GraphicObject[] objects = new GraphicObject[2];


        GraphicObject o = new Line();
        GraphicObject o1 = new Rectangle();

        objects[0]=o;
        objects[1]=o1;

        for(GraphicObject go : objects) {
            //go.test()
            if (go instanceof Line) {
                ((Line) go).test();
            }
            go.moveTo(8,9);
        }
    }
}
