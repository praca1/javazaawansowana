package annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;

public class ProcessAnnotationExample
{
    public static void main(String[] args) throws NoSuchMethodException, SecurityException
    {
        new Student();
        Class<Student> demoClassObj = Student.class;
        readAnnotationOn(demoClassObj);
        Method method = demoClassObj.getMethod("prepareStudentSummary", new Class[]{});
        readAnnotationOn(method);
    }

    static void readAnnotationOn(AnnotatedElement element)
    {
        try
        {
            System.out.println("\n Finding annotations on " + element.getClass().getName());
            Annotation[] annotations = element.getAnnotations();
            for (Annotation annotation : annotations)
            {
                if (annotation instanceof StudentDetailsAnnotation)
                {
                    StudentDetailsAnnotation studentInfo = (StudentDetailsAnnotation) annotation;
                    System.out.println("Author :" + studentInfo.studentName());
                    System.out.println("Address :" + studentInfo.studentAddress());
                    System.out.println("University :" + studentInfo.studentUniversity());
                    System.out.println("Age :" + studentInfo.studentAge());

                } else{
                    System.out.println(annotation.toString());
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
