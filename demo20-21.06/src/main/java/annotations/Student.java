package annotations;

@Deprecated
public class Student {
    public static void main(String[] args) {

    }


    @StudentDetailsAnnotation(
            studentName="Piotr",
            studentAddress="Warsaw, Poland"
    )
    public void prepareStudentSummary(){

        System.out.println("Summary fo the Student");
    }


}
