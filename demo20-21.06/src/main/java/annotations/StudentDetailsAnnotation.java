package annotations;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME) // Adnotacja będzie dostępna w fazie Runtime
@Inherited
public @interface StudentDetailsAnnotation {
    int studentAge() default 18;
    String studentName();
    String studentAddress();
    String studentUniversity() default "PW";
}
