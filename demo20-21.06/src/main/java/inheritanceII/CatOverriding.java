package inheritanceII;

public class CatOverriding {
    public static void main(String[] args) {
        Cat myCat = new Cat();
        myCat.testInstanceMethod();

        Animal myAnimal = myCat;
        myAnimal.testInstanceMethod();

    }
}
