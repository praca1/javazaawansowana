package statics;

public class Car {

    private int cadence;
    private int gear;
    private int speed;

    private int id;

    private static int numberOfCars = 0;


    public Car(int startCadence,
                   int startSpeed,
                   int startGear) {
        gear = startGear;
        cadence = startCadence;
        speed = startSpeed;

        id = ++numberOfCars;
    }

    public int getID() {
        return id;
    }

    public static int getNumberOfCars() {
        return numberOfCars;
    }

    public int getCadence() {
        return cadence;
    }

    public void setCadence(int newValue) {
        cadence = newValue;
    }

    public int getGear(){
        return gear;
    }

    public void setGear(int newValue) {
        gear = newValue;
    }

    public int getSpeed() {
        return speed;
    }

    public void applyBrake(int decrement) {
        speed -= decrement;
    }

    public void speedUp(int increment) {
        speed += increment;
    }
}