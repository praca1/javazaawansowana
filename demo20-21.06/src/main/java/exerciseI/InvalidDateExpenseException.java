package exerciseII;

public class InvalidDateExpenseException extends ExpenseException {
    public InvalidDateExpenseException(String message) {
        super(message);
    }
}
