package exerciseII;

public class InvalidCommentExpenseException extends ExpenseException {
    public InvalidCommentExpenseException(String message) {
        super(message);
    }
}
