package exerciseII;

import java.time.LocalDate;

public class Expense {

    private double amount;
    private LocalDate date;
    private String place;
    private String comment;

    public Expense(double amount, LocalDate date, String place, String comment) throws ExpenseException {
        if (amount < 0) {
            throw new InvalidAmountExpenseException("Invalid amount - cannot be smaller than 0");
        }  else if (date.isAfter(LocalDate.now())) {
            throw new InvalidAmountExpenseException("Invalid date - cannot be in future");
        } else if (comment == null || comment.trim().isEmpty()) {
            throw new InvalidCommentExpenseException("Invalid comment - cannot be empty or null");
        } else if (place == null || place.trim().isEmpty()) {
            throw new InvalidPlaceExpenseException("Invalid place - cannot be empty or null");
        } else {
            this.amount = amount;
            this.date = date;
            this.place = place;
            this.comment = comment;
        }
    }

    @Override
    public String toString() {
        return "Expense{" +
                "amount=" + amount +
                ", date=" + date +
                ", place='" + place + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
