package exerciseII;

import exerciseIII.ExceptionApp;

public abstract class ExpenseException extends Exception {
    public ExpenseException(String message) {
        super(message);
    }
}
