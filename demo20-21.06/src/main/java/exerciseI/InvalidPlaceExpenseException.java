package exerciseII;

public class InvalidPlaceExpenseException extends ExpenseException {
    public InvalidPlaceExpenseException(String message) {
        super(message);
    }
}
