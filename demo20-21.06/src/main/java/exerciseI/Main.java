package exerciseII;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        ExpenseService es = new ExpenseService();
        Expense e=null;
        try {
            e = new Expense(5.0, LocalDate.of(2015, 12, 31), "Gdansk", "Dorsz");
        } catch (ExpenseException ex) {
            System.out.println(ex.getMessage());
        }
        es.addExpense(e);
        System.out.println(es.toString());

    }
}
