package exerciseII;

public class InvalidAmountExpenseException extends ExpenseException {
    public InvalidAmountExpenseException(String message) {
        super(message);
    }
}
