package exerciseII;

import java.util.ArrayList;
import java.util.List;

public class ExpenseService {

    List<Expense> expenses;

    public ExpenseService() {
        expenses = new ArrayList<>();
    }

    public void addExpense(Expense expense){
        expenses.add(expense);
    }
    public List<Expense> getExpenses(){
        return expenses;
    }

    @Override
    public String toString() {
        return "ExpenseService{" +
                "expenses=" + expenses +
                '}';
    }
}
